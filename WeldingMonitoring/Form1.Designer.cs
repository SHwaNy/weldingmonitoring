﻿namespace WeldingMonitoring
{
    partial class Form1
    {
        /// <summary>
        /// 필수 디자이너 변수입니다.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 사용 중인 모든 리소스를 정리합니다.
        /// </summary>
        /// <param name="disposing">관리되는 리소스를 삭제해야 하면 true이고, 그렇지 않으면 false입니다.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form 디자이너에서 생성한 코드

        /// <summary>
        /// 디자이너 지원에 필요한 메서드입니다. 
        /// 이 메서드의 내용을 코드 편집기로 수정하지 마세요.
        /// </summary>
        private void InitializeComponent()
        {
            System.Windows.Forms.DataVisualization.Charting.ChartArea chartArea1 = new System.Windows.Forms.DataVisualization.Charting.ChartArea();
            System.Windows.Forms.DataVisualization.Charting.Legend legend1 = new System.Windows.Forms.DataVisualization.Charting.Legend();
            System.Windows.Forms.DataVisualization.Charting.Series series1 = new System.Windows.Forms.DataVisualization.Charting.Series();
            this.lblPort = new System.Windows.Forms.Label();
            this.cbComPort = new System.Windows.Forms.ComboBox();
            this.lblBuadRate = new System.Windows.Forms.Label();
            this.cbBaudRate = new System.Windows.Forms.ComboBox();
            this.lblDataSize = new System.Windows.Forms.Label();
            this.cbDataSize = new System.Windows.Forms.ComboBox();
            this.lblParity = new System.Windows.Forms.Label();
            this.cbParity = new System.Windows.Forms.ComboBox();
            this.lblHandShake = new System.Windows.Forms.Label();
            this.cbHandShake = new System.Windows.Forms.ComboBox();
            this.btConnectControl = new System.Windows.Forms.Button();
            this.tbSendMessage = new System.Windows.Forms.TextBox();
            this.btSendMessage = new System.Windows.Forms.Button();
            this.lblRecvMessage = new System.Windows.Forms.Label();
            this.tbRecvMessage = new System.Windows.Forms.TextBox();
            this.dgvDataLog = new System.Windows.Forms.DataGridView();
            this.Column1 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column2 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column3 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column4 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column5 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.Column6 = new System.Windows.Forms.DataGridViewTextBoxColumn();
            this.lblCulcData = new System.Windows.Forms.Label();
            this.tbCulcData = new System.Windows.Forms.TextBox();
            this.chtData = new System.Windows.Forms.DataVisualization.Charting.Chart();
            ((System.ComponentModel.ISupportInitialize)(this.dgvDataLog)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.chtData)).BeginInit();
            this.SuspendLayout();
            // 
            // lblPort
            // 
            this.lblPort.AutoSize = true;
            this.lblPort.Location = new System.Drawing.Point(44, 9);
            this.lblPort.Name = "lblPort";
            this.lblPort.Size = new System.Drawing.Size(27, 12);
            this.lblPort.TabIndex = 5;
            this.lblPort.Text = "Port";
            // 
            // cbComPort
            // 
            this.cbComPort.FormattingEnabled = true;
            this.cbComPort.Location = new System.Drawing.Point(11, 24);
            this.cbComPort.Name = "cbComPort";
            this.cbComPort.Size = new System.Drawing.Size(91, 20);
            this.cbComPort.TabIndex = 6;
            this.cbComPort.SelectedIndexChanged += new System.EventHandler(this.cbComPort_SelectedIndexChanged);
            // 
            // lblBuadRate
            // 
            this.lblBuadRate.AutoSize = true;
            this.lblBuadRate.Location = new System.Drawing.Point(124, 9);
            this.lblBuadRate.Name = "lblBuadRate";
            this.lblBuadRate.Size = new System.Drawing.Size(63, 12);
            this.lblBuadRate.TabIndex = 5;
            this.lblBuadRate.Text = "Baud Rate";
            // 
            // cbBaudRate
            // 
            this.cbBaudRate.FormattingEnabled = true;
            this.cbBaudRate.Items.AddRange(new object[] {
            "115200",
            "19200",
            "38400",
            "57600",
            "9600"});
            this.cbBaudRate.Location = new System.Drawing.Point(108, 24);
            this.cbBaudRate.Name = "cbBaudRate";
            this.cbBaudRate.Size = new System.Drawing.Size(91, 20);
            this.cbBaudRate.TabIndex = 6;
            this.cbBaudRate.SelectedIndexChanged += new System.EventHandler(this.cbBaudRate_SelectedIndexChanged);
            // 
            // lblDataSize
            // 
            this.lblDataSize.AutoSize = true;
            this.lblDataSize.Location = new System.Drawing.Point(221, 9);
            this.lblDataSize.Name = "lblDataSize";
            this.lblDataSize.Size = new System.Drawing.Size(59, 12);
            this.lblDataSize.TabIndex = 5;
            this.lblDataSize.Text = "Data Size";
            // 
            // cbDataSize
            // 
            this.cbDataSize.FormattingEnabled = true;
            this.cbDataSize.Items.AddRange(new object[] {
            "8",
            "7",
            "6"});
            this.cbDataSize.Location = new System.Drawing.Point(205, 24);
            this.cbDataSize.Name = "cbDataSize";
            this.cbDataSize.Size = new System.Drawing.Size(91, 20);
            this.cbDataSize.TabIndex = 6;
            this.cbDataSize.SelectedIndexChanged += new System.EventHandler(this.cbDataSize_SelectedIndexChanged);
            // 
            // lblParity
            // 
            this.lblParity.AutoSize = true;
            this.lblParity.Location = new System.Drawing.Point(330, 9);
            this.lblParity.Name = "lblParity";
            this.lblParity.Size = new System.Drawing.Size(37, 12);
            this.lblParity.TabIndex = 5;
            this.lblParity.Text = "Parity";
            // 
            // cbParity
            // 
            this.cbParity.FormattingEnabled = true;
            this.cbParity.Items.AddRange(new object[] {
            "none",
            "even",
            "mark",
            "odd",
            "space"});
            this.cbParity.Location = new System.Drawing.Point(302, 24);
            this.cbParity.Name = "cbParity";
            this.cbParity.Size = new System.Drawing.Size(91, 20);
            this.cbParity.TabIndex = 6;
            this.cbParity.SelectedIndexChanged += new System.EventHandler(this.cbParity_SelectedIndexChanged);
            // 
            // lblHandShake
            // 
            this.lblHandShake.AutoSize = true;
            this.lblHandShake.Location = new System.Drawing.Point(407, 9);
            this.lblHandShake.Name = "lblHandShake";
            this.lblHandShake.Size = new System.Drawing.Size(73, 12);
            this.lblHandShake.TabIndex = 5;
            this.lblHandShake.Text = "Hand Shake";
            // 
            // cbHandShake
            // 
            this.cbHandShake.FormattingEnabled = true;
            this.cbHandShake.Items.AddRange(new object[] {
            "none",
            "Xon/Xoff",
            "request to send",
            "request to send Xon/Xoff"});
            this.cbHandShake.Location = new System.Drawing.Point(399, 24);
            this.cbHandShake.Name = "cbHandShake";
            this.cbHandShake.Size = new System.Drawing.Size(91, 20);
            this.cbHandShake.TabIndex = 6;
            this.cbHandShake.SelectedIndexChanged += new System.EventHandler(this.cbHandShake_SelectedIndexChanged);
            // 
            // btConnectControl
            // 
            this.btConnectControl.Location = new System.Drawing.Point(11, 51);
            this.btConnectControl.Name = "btConnectControl";
            this.btConnectControl.Size = new System.Drawing.Size(91, 23);
            this.btConnectControl.TabIndex = 7;
            this.btConnectControl.Text = "Connect";
            this.btConnectControl.UseVisualStyleBackColor = true;
            this.btConnectControl.Click += new System.EventHandler(this.btConnectControl_Click);
            // 
            // tbSendMessage
            // 
            this.tbSendMessage.Location = new System.Drawing.Point(108, 53);
            this.tbSendMessage.Name = "tbSendMessage";
            this.tbSendMessage.Size = new System.Drawing.Size(285, 21);
            this.tbSendMessage.TabIndex = 8;
            this.tbSendMessage.TextChanged += new System.EventHandler(this.tbSendMessage_TextChanged);
            // 
            // btSendMessage
            // 
            this.btSendMessage.Location = new System.Drawing.Point(399, 51);
            this.btSendMessage.Name = "btSendMessage";
            this.btSendMessage.Size = new System.Drawing.Size(91, 23);
            this.btSendMessage.TabIndex = 7;
            this.btSendMessage.Text = "Send";
            this.btSendMessage.UseVisualStyleBackColor = true;
            this.btSendMessage.Click += new System.EventHandler(this.btSendMessage_Click);
            // 
            // lblRecvMessage
            // 
            this.lblRecvMessage.AutoSize = true;
            this.lblRecvMessage.Location = new System.Drawing.Point(9, 81);
            this.lblRecvMessage.Name = "lblRecvMessage";
            this.lblRecvMessage.Size = new System.Drawing.Size(103, 12);
            this.lblRecvMessage.TabIndex = 9;
            this.lblRecvMessage.Text = "ReceiveMessage";
            // 
            // tbRecvMessage
            // 
            this.tbRecvMessage.Location = new System.Drawing.Point(11, 99);
            this.tbRecvMessage.Multiline = true;
            this.tbRecvMessage.Name = "tbRecvMessage";
            this.tbRecvMessage.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.tbRecvMessage.Size = new System.Drawing.Size(479, 83);
            this.tbRecvMessage.TabIndex = 10;
            // 
            // dgvDataLog
            // 
            this.dgvDataLog.AutoSizeColumnsMode = System.Windows.Forms.DataGridViewAutoSizeColumnsMode.Fill;
            this.dgvDataLog.AutoSizeRowsMode = System.Windows.Forms.DataGridViewAutoSizeRowsMode.AllHeaders;
            this.dgvDataLog.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
            this.dgvDataLog.Columns.AddRange(new System.Windows.Forms.DataGridViewColumn[] {
            this.Column1,
            this.Column2,
            this.Column3,
            this.Column4,
            this.Column5,
            this.Column6});
            this.dgvDataLog.Location = new System.Drawing.Point(12, 195);
            this.dgvDataLog.Name = "dgvDataLog";
            this.dgvDataLog.RowTemplate.Height = 23;
            this.dgvDataLog.Size = new System.Drawing.Size(479, 131);
            this.dgvDataLog.TabIndex = 11;
            // 
            // Column1
            // 
            this.Column1.HeaderText = "AccX";
            this.Column1.Name = "Column1";
            // 
            // Column2
            // 
            this.Column2.HeaderText = "AccY";
            this.Column2.Name = "Column2";
            // 
            // Column3
            // 
            this.Column3.HeaderText = "AccZ";
            this.Column3.Name = "Column3";
            // 
            // Column4
            // 
            this.Column4.HeaderText = "GyroX";
            this.Column4.Name = "Column4";
            // 
            // Column5
            // 
            this.Column5.HeaderText = "GyroY";
            this.Column5.Name = "Column5";
            // 
            // Column6
            // 
            this.Column6.HeaderText = "GyroZ";
            this.Column6.Name = "Column6";
            // 
            // lblCulcData
            // 
            this.lblCulcData.AutoSize = true;
            this.lblCulcData.Location = new System.Drawing.Point(9, 334);
            this.lblCulcData.Name = "lblCulcData";
            this.lblCulcData.Size = new System.Drawing.Size(93, 12);
            this.lblCulcData.TabIndex = 9;
            this.lblCulcData.Text = "CalculationData";
            // 
            // tbCulcData
            // 
            this.tbCulcData.Location = new System.Drawing.Point(11, 352);
            this.tbCulcData.Multiline = true;
            this.tbCulcData.Name = "tbCulcData";
            this.tbCulcData.ScrollBars = System.Windows.Forms.ScrollBars.Vertical;
            this.tbCulcData.Size = new System.Drawing.Size(479, 122);
            this.tbCulcData.TabIndex = 10;
            // 
            // chtData
            // 
            this.chtData.BorderlineColor = System.Drawing.Color.Black;
            this.chtData.BorderlineDashStyle = System.Windows.Forms.DataVisualization.Charting.ChartDashStyle.Solid;
            this.chtData.BorderlineWidth = 2;
            chartArea1.Name = "ChartArea1";
            this.chtData.ChartAreas.Add(chartArea1);
            legend1.Name = "Legend1";
            this.chtData.Legends.Add(legend1);
            this.chtData.Location = new System.Drawing.Point(506, 9);
            this.chtData.Name = "chtData";
            series1.ChartArea = "ChartArea1";
            series1.Legend = "Legend1";
            series1.Name = "Series1";
            this.chtData.Series.Add(series1);
            this.chtData.Size = new System.Drawing.Size(500, 465);
            this.chtData.TabIndex = 12;
            this.chtData.Text = "chtData";
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(7F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(1017, 486);
            this.Controls.Add(this.chtData);
            this.Controls.Add(this.dgvDataLog);
            this.Controls.Add(this.tbCulcData);
            this.Controls.Add(this.lblCulcData);
            this.Controls.Add(this.tbRecvMessage);
            this.Controls.Add(this.lblRecvMessage);
            this.Controls.Add(this.tbSendMessage);
            this.Controls.Add(this.btSendMessage);
            this.Controls.Add(this.btConnectControl);
            this.Controls.Add(this.cbHandShake);
            this.Controls.Add(this.lblHandShake);
            this.Controls.Add(this.cbParity);
            this.Controls.Add(this.lblParity);
            this.Controls.Add(this.cbDataSize);
            this.Controls.Add(this.lblDataSize);
            this.Controls.Add(this.cbBaudRate);
            this.Controls.Add(this.lblBuadRate);
            this.Controls.Add(this.cbComPort);
            this.Controls.Add(this.lblPort);
            this.Name = "Form1";
            this.Text = "Filter";
            this.Load += new System.EventHandler(this.Form1_Load);
            ((System.ComponentModel.ISupportInitialize)(this.dgvDataLog)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.chtData)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.Label lblPort;
        private System.Windows.Forms.ComboBox cbComPort;
        private System.Windows.Forms.Label lblBuadRate;
        private System.Windows.Forms.ComboBox cbBaudRate;
        private System.Windows.Forms.Label lblDataSize;
        private System.Windows.Forms.ComboBox cbDataSize;
        private System.Windows.Forms.Label lblParity;
        private System.Windows.Forms.ComboBox cbParity;
        private System.Windows.Forms.Label lblHandShake;
        private System.Windows.Forms.ComboBox cbHandShake;
        private System.Windows.Forms.Button btConnectControl;
        private System.Windows.Forms.TextBox tbSendMessage;
        private System.Windows.Forms.Button btSendMessage;
        private System.Windows.Forms.Label lblRecvMessage;
        private System.Windows.Forms.TextBox tbRecvMessage;
        private System.Windows.Forms.DataGridView dgvDataLog;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column1;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column2;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column3;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column4;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column5;
        private System.Windows.Forms.DataGridViewTextBoxColumn Column6;
        private System.Windows.Forms.Label lblCulcData;
        private System.Windows.Forms.TextBox tbCulcData;
        private System.Windows.Forms.DataVisualization.Charting.Chart chtData;
    }
}

