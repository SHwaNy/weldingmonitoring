﻿using System;
using System.IO.Ports;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Windows.Forms.DataVisualization.Charting;

namespace WeldingMonitoring
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
            Init();
        }

        Random R = new Random();
        Series sSin;
        private uint count;

        public const double FS_SEL = 131;
        public const double RADIANS_TO_DEGREES = 57.29582;
        public const double ACC_SENS = 8192.0;
        public const double GYRO_SENS = 65.536;
        public const double G = 1;
        public const double dt = 0.01;              // 10ms sample rate

        private double[] Acc = new double[3];
        private double[] Gyro = new double[3];

        private double pitchAcc;
        private double rollAcc;

        private double gyroPHat;
        private double gyroQHat;

        private double pitchDot;
        private double rollDot;
        private double yawDot;

        private double pitch;
        private double roll;
        private double yaw;

        private double prevPitch;
        private double prevRoll;
        private double prevYaw;

        private double prevGyroP;
        private double prevGyroQ;

        private double prevDelPitch;
        private double prevDelRoll;


        public void Init()
        {
            count = 0;

            Acc[0] = 0;
            Acc[1] = 0;
            Acc[2] = 0;

            Gyro[0] = 0;
            Gyro[1] = 0;
            Gyro[2] = 0;

            pitchDot = 0;
            rollDot = 0;
            yawDot = 0;

            pitch = 0;
            roll = 0;
            yaw = 0;

            prevPitch = 0;
            prevRoll = 0;
            prevYaw = 0;

            prevGyroP = 0;
            prevGyroQ = 0;

            prevDelPitch = 0;
            prevDelRoll = 0;
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            cbComPort.DataSource = SerialPort.GetPortNames();
            cbBaudRate.SelectedIndex = 0;
            cbDataSize.SelectedIndex = 0;
            cbParity.SelectedIndex = 0;
            cbHandShake.SelectedIndex = 0;

            chtData.Series.Clear();
            sSin = chtData.Series.Add("sin");
            chtData.ChartAreas[0].AxisX.Minimum = 1;
            sSin.ChartType = SeriesChartType.Line;
        }

        private bool culEulerAccel()
        {
            pitchAcc = Math.Asin(Acc[0] / G);
            rollAcc = Math.Asin(-Acc[1] / (G * Math.Cos(pitchAcc)));

            return true;
        }

        private bool culBodyToInertial()
        {
            double sinPitch, cosPitch;
            double cosRoll, tanRoll;

            sinPitch = Math.Sin(prevPitch);
            cosPitch = Math.Cos(prevPitch);
            cosRoll = Math.Cos(prevRoll);
            tanRoll = Math.Tan(prevRoll);

            pitchDot = Gyro[0] + (Gyro[1] * sinPitch * tanRoll) + (Gyro[2] * cosPitch * tanRoll);
            rollDot = (Gyro[1] * cosPitch) - (Gyro[2] * sinPitch);
            yawDot = (Gyro[1] * sinPitch / cosRoll) - (Gyro[2] * cosPitch / cosRoll);

            return true;
        }

        private bool PILawPitch()
        {
            double delPitch;
  
            delPitch = pitch - pitchAcc;

            gyroPHat = prevGyroP + (0.1415 * delPitch) - (0.1414 * prevDelPitch);

            prevGyroP = gyroPHat;
            prevDelPitch = delPitch;

            return true;
        }

        private bool PILawRoll()
        {
            double delRoll;

            delRoll = pitch - pitchAcc;

            gyroQHat = prevGyroQ + (0.1415 * delRoll) - (0.1414 * prevDelRoll);

            prevGyroQ = gyroQHat;
            prevDelRoll = delRoll;

            return true;
        }

        private bool culComplementaryFilter()
        {

            culEulerAccel();
            culBodyToInertial();

            pitch = prevPitch + dt * (pitchDot - gyroPHat);
            roll = prevRoll + dt * (rollDot - gyroQHat);
            yaw = prevYaw + dt * (yawDot);

            PILawPitch();
            PILawRoll();

            prevPitch = pitch;
            prevRoll = roll;
            prevYaw = yaw;
            
            return true;
        }

        private SerialPort _Port;
        private SerialPort Port
        {
            get
            {
                if (_Port == null)
                {
                    _Port = new SerialPort();
                
                    _Port.PortName = "COM1";
                    _Port.BaudRate = 115200;
                    _Port.DataBits = 8;
                    _Port.Parity = Parity.None;
                    _Port.Handshake = Handshake.None;
                    _Port.StopBits = StopBits.One;
                    _Port.Encoding = Encoding.UTF8;

                    _Port.DataReceived += Port_DataReceived;
                }
                return _Port;
            }
        }
        private delegate void DataDelegate(string sData);

        private int flagSync = 0;

        private void Port_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            List<byte> recvDataList = new List<byte>();
            String msg;

            /*if (temp == 0)
            {
                for(int i = 0; i < Port.BytesToRead; i++)
                {
                    msg = Convert.ToString(Port.ReadByte());
                    if(msg == "#")
               
            
            }
            }*/
            Thread.Sleep(1);
            msg = Port.ReadExisting();
            String text = msg;

            this.Invoke(new EventHandler(delegate
            {
                DataGridViewRow row = (DataGridViewRow)dgvDataLog.Rows[0].Clone();
                string[] split_text;

                Int32 search1 = text.IndexOf("$");
                Int32 search2 = text.IndexOf("#");
                if (search1 >= 0 && search2 > 1 && flagSync != 0)
                {
                    text = text.Substring(search1 + 1, search2 - 1);
                    split_text = text.Split(',');
                    if (split_text[0] != "")
                    {
                        StringsRecv = "실행";
                        for (int i = 0; i < split_text.Length; i++)
                        {
                            row.Cells[i].Value = split_text[i];
                            if (i <= 2)
                            {
                                Acc[i] = Convert.ToDouble(split_text[i]);
                            }
                            else
                            {
                                Gyro[(i % 3)] = Convert.ToDouble(split_text[i]);
                            }
                        }
                        dgvDataLog.Rows.Add(row);
                    }
                }
                count++;
                flagSync = 1;

                StringsRecv = String.Format("[RECV] {0}", msg);
            }));

            this.Invoke(new EventHandler(delegate
            {
                culComplementaryFilter();
                StringsCulc = String.Format("[CULC] {0}, {0}, {0}", pitch * 180 / Math.PI, roll * 180 / Math.PI, yaw * 180 / Math.PI);
                sSin.Points.AddXY(count, (2 * (R.NextDouble() - 0.5) * 5));
            }));
        }

        private Boolean IsOpen
        {
            get { return Port.IsOpen; }
            set
            {
                if (value)
                {
                    StringsRecv = "연결 됨";
                    btConnectControl.Text = "연결 끊기";
                    tbRecvMessage.Enabled = true;
                    tbSendMessage.Enabled = true;
                    btSendMessage.Enabled = true;
                }
                else
                {
                    StringsRecv = "연결 해제됨";
                    btConnectControl.Text = "연결";
                    tbRecvMessage.Enabled = false;
                    tbSendMessage.Enabled = false;
                    btSendMessage.Enabled = false;
                }
            }
        }

        private StringBuilder _StringsRecv;
        private String StringsRecv
        {
            set
            {
                if (_StringsRecv == null)
                    _StringsRecv = new StringBuilder(1024);
                if (_StringsRecv.Length >= (1024 - value.Length))
                    _StringsRecv.Clear();
                _StringsRecv.AppendLine(value);
                tbRecvMessage.Text = _StringsRecv.ToString();
            }
        }

        private StringBuilder _StringsCulc;
        private String StringsCulc
        {
            set
            {
                if (_StringsCulc == null)
                    _StringsCulc = new StringBuilder(1024);
                if (_StringsCulc.Length >= (1024 - value.Length))
                    _StringsCulc.Clear();
                _StringsCulc.AppendLine(value);
                tbCulcData.Text = _StringsCulc.ToString();
            }
        }

        private void btnClose_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void tbSendMessage_TextChanged(object sender, EventArgs e)
        {
            tbRecvMessage.SelectionStart = tbRecvMessage.Text.Length;
            tbRecvMessage.ScrollToCaret();
        }

        private void btSendMessage_Click(object sender, EventArgs e)
        {
            String text = tbSendMessage.Text.Trim();
            if (String.IsNullOrEmpty(text)) return;
            try
            {
                Port.WriteLine(text);
                StringsRecv = String.Format("[SEND] {0}", text);
            }
            catch (Exception ex) { StringsRecv = String.Format("[ERR] {0}", ex.Message); }
        }

        private void btConnectControl_Click(object sender, EventArgs e)
        {
            if (!Port.IsOpen)
            {
                Port.PortName = cbComPort.SelectedItem.ToString();
                Port.BaudRate = Convert.ToInt32(cbBaudRate.SelectedItem);
                Port.DataBits = Convert.ToInt32(cbDataSize.SelectedItem);
                Port.Parity = (Parity)cbParity.SelectedIndex;
                Port.Handshake = (Handshake)cbHandShake.SelectedIndex;

                try
                {
                    Port.Open();
                }
                catch (Exception ex) { StringsRecv = String.Format("[ERR] {0}", ex.Message); }
            }
            else
            {
                Port.Close();
            }
            IsOpen = Port.IsOpen;
        }

        private void cbComPort_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void cbBaudRate_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void cbDataSize_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void cbParity_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void cbHandShake_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

    }
}